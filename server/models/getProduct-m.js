import mysql from "mysql2/promise";
import { dbPool } from "../dbconfig.js";

// sql.on("error", (err) => {
//   throw err;
// });
// MODEL: dùng để tạo truy vấn
export default class Product {
  constructor(product_id) {
    this.product_id = product_id;
  }

  static async getProductDetail(id) {
    try {
      const query = `SELECT * 
                     FROM product WHERE product_id = ?`;

      const [results] = await dbPool.query(query, [id]);
      if (results.length > 0) {
        return results[0]; // trả về 1 item duy nhất theo id
      } else {
        throw new Error("Product item not found");
      }
    } catch (error) {
      console.error("Error:", error);
      throw error;
    }
  }
  static async getProduct() {
    try {
      const query = `SELECT product_id, product_name, img FROM product`;
      const [results] = await dbPool.query(query);

      return results;
    } catch (error) {
      console.error("Error:", error);
      throw error;
    }
  }
  static async getProductNutrients(id) {
    try {
      const query = `SELECT * FROM nutrients WHERE product_id = ${id}`;
      const [results] = await dbPool.query(query);

      return results[0];
    } catch (error) {
      console.error("Error:", error);
      throw error;
    }
  }
  static async getCategory() {
    try {
      const query = `SELECT * FROM category`;
      const [results] = await dbPool.query(query);

      return results;
    } catch (error) {
      console.error("Error:", error);
      throw error;
    }
  }
  static async getProductByIdOrCategory(
    id,
    level0,
    level1 = null,
    level2 = null
  ) {
    try {
      let query = `SELECT * FROM category WHERE 1=1`;
      const params = [];

      if (id) {
        query += ` AND product_id = ?`;
        params.push(id);
      } else {
        if (level0) {
          query += ` AND level_0 = ?`;
          params.push(level0);
        }
        if (level1) {
          query += ` AND level_1 = ?`;
          params.push(level1);
        }
        if (level2) {
          query += ` AND level_2 = ?`;
          params.push(level2);
        }
      }

      console.log("Query:", query); // Log the query
      console.log("Params:", params); // Log the parameters

      const [results] = await dbPool.query(query, params);
      return results;
    } catch (error) {
      console.error("Error:", error);
      throw error;
    }
  }
}
